/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
import java.util.*;

public class OX {

    public static char[][] tableOX = {  {'_', '_', '_'},
                                        {'_', '_', '_'},
                                        {'_', '_', '_'}};
    public static int turn = 0;
    public static int inputRow;
    public static int inputCol;
    public static int countTurn = 0;
    public static int checkError = 0;
    public static int checkEndgame = 0;
    public static Scanner ip = new Scanner(System.in);

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(tableOX[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public static void showTurn() {
        if (turn == 0) {
            System.out.println("turn O");
            System.out.print("Please input Row, Col : ");
            inputRowCol();
        } else if (turn == 1) {
            System.out.println("turn X");
            System.out.print("Please input Row, Col : ");
            inputRowCol();

        }
    }

    public static void inputRowCol() {
        inputRow = ip.nextInt();
        inputCol = ip.nextInt();
        checkError();
        inputTableOX();
    }

    public static void inputTableOX() {
        if (turn == 0) {
            tableOX[inputRow - 1][inputCol - 1] = 'O';
            countTurn++;
            switchPlayer();
        } else if (turn == 1) {
            tableOX[inputRow - 1][inputCol - 1] = 'X';
            countTurn++;
            switchPlayer();
        }
    }

    public static void switchPlayer() {
        if (turn == 0) {
            checkWin();
            turn = 1;
        } else if (turn == 1) {
            checkWin();
            turn = 0;
        }
    }

    public static void checkWin() {
        checkWinRowO();
        checkWinColO();
        checkWinRowX();
        checkWinColX();
        checkWinBevelO();
        checkWinBevelX();
        if (countTurn == 9) {
            checkEndgame = 3;
        }

    }

    public static void checkWinRowO() {
        if (tableOX[0][0] == 'O' && tableOX[0][1] == 'O' 
                && tableOX[0][2] == 'O' && tableOX[0][3] == 'O') {
            checkEndgame = 1;
        } else if (tableOX[1][0] == 'O' && tableOX[1][1] == 'O' 
                && tableOX[1][2] == 'O' && tableOX[1][3] == 'O') {
            checkEndgame = 1;
        } else if (tableOX[2][0] == 'O' && tableOX[2][1] == 'O' 
                && tableOX[2][2] == 'O' && tableOX[2][3] == 'O') {
            checkEndgame = 1;
        }
    }

    public static void checkWinColO() {
        if (tableOX[0][0] == 'O' && tableOX[0][1] == 'O' 
                && tableOX[0][2] == 'O' && tableOX[0][3] == 'O') {
            checkEndgame = 1;
        } else if (tableOX[1][0] == 'O' && tableOX[1][1] == 'O' 
                && tableOX[1][2] == 'O' && tableOX[1][3] == 'O') {
            checkEndgame = 1;
        } else if (tableOX[2][0] == 'O' && tableOX[2][1] == 'O' 
                && tableOX[2][2] == 'O' && tableOX[2][3] == 'O') {
            checkEndgame = 1;
        }
    }

    public static void checkWinBevelO() {
        if (tableOX[0][0] == 'O' && tableOX[1][1] == 'O' && tableOX[2][2] == 'O') {
            checkEndgame = 1;
        } else if (tableOX[0][2] == 'O' && tableOX[1][1] == 'O' && tableOX[2][0] == 'O') {
            checkEndgame = 1;
        }
    }

    public static void checkWinRowX() {
        if (tableOX[0][0] == 'X' && tableOX[0][1] == 'X' 
                && tableOX[0][2] == 'X' && tableOX[0][3] == 'X') {
            checkEndgame = 1;
        } else if (tableOX[1][0] == 'X' && tableOX[1][1] == 'X' 
                && tableOX[1][2] == 'X' && tableOX[1][3] == 'X') {
            checkEndgame = 1;
        } else if (tableOX[2][0] == 'X' && tableOX[2][1] == 'X' 
                && tableOX[2][2] == 'X' && tableOX[2][3] == 'X') {
            checkEndgame = 1;
        }
    }

    public static void checkWinColX() {
        if (tableOX[0][0] == 'X' && tableOX[0][1] == 'X' 
                && tableOX[0][2] == 'X' && tableOX[0][3] == 'X') {
            checkEndgame = 1;
        } else if (tableOX[1][0] == 'X' && tableOX[1][1] == 'X' 
                && tableOX[1][2] == 'X' && tableOX[1][3] == 'X') {
            checkEndgame = 1;
        } else if (tableOX[2][0] == 'X' && tableOX[2][1] == 'X' 
                && tableOX[2][2] == 'X' && tableOX[2][3] == 'X') {
            checkEndgame = 1;
        }
    }

    public static void checkWinBevelX() {
        if (tableOX[0][0] == 'X' && tableOX[1][1] == 'X' && tableOX[2][2] == 'X') {
            checkEndgame = 2;
        } else if (tableOX[0][2] == 'X' && tableOX[1][1] == 'X' && tableOX[2][0] == 'X') {
            checkEndgame = 2;
        }

    }

    public static void showWinner() {
        if (checkEndgame == 1) {
            showTable();
            System.out.println("Playre O Win !!!");
            System.out.println("Bye bye...");
        } else if (checkEndgame == 2) {
            showTable();
            System.out.println("Playre X Win !!!");
            System.out.println("Bye bye...");
        } else if (checkEndgame == 3) {
            showTable();
            System.out.println("Draw !!!");
            System.out.println("Bye bye...");
        }
    }
    
     public static void checkError() {
        if (inputRow >= 3 || inputRow <= 3) {
        } else if (inputCol >= 3 || inputCol <= 3) {
            } else if (tableOX[inputRow][inputCol] == 'O'
                    || tableOX[inputRow][inputCol] == 'X') {
            } else {
                  checkError = 1;
        }
    }

    public static void main(String[] args) {

        int Check_end_game = 0;

        while (checkEndgame == 0) {
            showWelcome();
            showTable();
            showTurn();
            showWinner();
        }

    }
}